﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleAI : MonoBehaviour {
    private Vector3 initialPos;
    public float speed = 1f;
    public int maxXPosition = 5;
    private Rigidbody rigidbody;
    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
        initialPos = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rigidbody.MovePosition(transform.position + transform.forward * Time.deltaTime);
        if (gameObject.transform.position.x >= maxXPosition)
        {
            transform.position = initialPos;
        }
    }
    }
